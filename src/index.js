const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

const app = express();
app.use(express.json());
app.use(cors());
dotenv.config();

const authRoute = require('./route/Auth.route');
const postRoute = require('./route/Post.route');
const userRoute = require('./route/User.route');
const commentRoute = require('./route/Comment.route');
const notificationRoute = require('./route/Notification.route');

app.use('/user', authRoute);
app.use('/post', postRoute);
app.use('/post/comment', commentRoute);
app.use('/user', userRoute);
app.use('/notification', notificationRoute);

mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.connection.on('connected', () => {
});

mongoose.connection.on('error', () => {
});

app.listen(process.env.PORT || 3001, () => {
});
