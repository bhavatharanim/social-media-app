exports.signupValidate = (req, res, next) => {
  const {
    name, email, password, gender,
  } = req.body;
  if (!email || !password || !name || !gender) {
    return res.status(422).json({ error: 'please add all the fields' });
  }
  const validLength = password.length >= 8;
  const hasLetter = /[a-zA-Z]/g.test(password);
  const hasNumber = /[0-9]/g.test(password);
  const isValidEmail = email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);
  if (!isValidEmail) {
    return res.status(422).json({ error: 'please enter correct email-id' });
  }
  if (!validLength) {
    return res.status(422).json({ error: 'password must be 8 characters in length' });
  }
  if (!hasLetter || !hasNumber) {
    return res.status(422).json({ error: 'password must contain letter and alaphabet' });
  }
  if (validLength && hasLetter && hasNumber) {
    next();
  }
};

exports.loginValidate = (req, res, next) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return res.status(422).json({ error: 'please add email or password' });
  }
  next();
};
