exports.updateValidate = (req, res, next) => {
  const {
    country,
    city,
    aboutMe,
  } = req.body;

  if (!country || !city || !aboutMe) {
    return res.status(422).json({ error: 'please add all the fields' });
  }

  const validLength = aboutMe.length >= 16;
  if (!validLength) {
    return res.status(422).json({ error: 'About me should contain atleast 16 characters' });
  }
  if (validLength) {
    next();
  }
};
