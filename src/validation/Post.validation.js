exports.createPostValidate = (req, res, next) => {
  const { title, body } = req.body;
  if (!title || !body) {
    return res.status(422).json({ error: 'please add all the details' });
  }
  const validLength = body.length >= 10;
  const hasLetter = /[a-zA-Z]/g.test(title);
  if (!validLength) {
    return res.status(422).json({ error: 'Description must be 10 characters in length' });
  }
  if (!hasLetter) {
    return res.status(422).json({ error: 'Title must contain some letter' });
  }
  if (validLength && hasLetter) {
    next();
  }
};
