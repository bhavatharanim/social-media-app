const mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema.Types;

const PostSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  body: {
    type: String,
    required: true,
  },
  photo: {
    type: String,
    default: 'no photo',
  },
  likes: [{ type: ObjectId, ref: 'Users' }],
  comments: [{ type: ObjectId, ref: 'Comments' }],
  postedBy: {
    type: ObjectId,
    ref: 'Users',
  },
}, { timestamps: true });
module.exports = mongoose.model('Posts', PostSchema);
