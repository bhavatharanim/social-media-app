const mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema.Types;

const NotificationSchema = new mongoose.Schema({
  requestedBy: {
    type: ObjectId,
    ref: 'Users',
  },
}, { timestamps: true });
module.exports = mongoose.model('Notifications', NotificationSchema);
