const mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema.Types;

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  gender: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  profileImage: {
    type: String,
    default: '',
  },
  city: {
    type: String,
    default: '',
  },
  country: {
    type: String,
    default: '',
  },
  aboutMe: {
    type: String,
    default: '',
  },
  followers: [{ type: ObjectId, ref: 'Users' }],
  following: [{ type: ObjectId, ref: 'Users' }],
  notifications: [{ type: ObjectId, ref: 'Notifications' }],
});
module.exports = mongoose.model('Users', UserSchema);
