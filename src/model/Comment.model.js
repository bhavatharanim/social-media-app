const mongoose = require('mongoose');

const { ObjectId } = mongoose.Schema.Types;

const CommentSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  postedBy: {
    type: ObjectId,
    ref: 'Users',
  },
  photo: {
    type: String,
    default: 'no photo',
  },
  likes: [{ type: ObjectId, ref: 'Users' }],
  comments: [{ type: ObjectId, ref: 'Comments' }],

}, { timestamps: true });
module.exports = mongoose.model('Comments', CommentSchema);
