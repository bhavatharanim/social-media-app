const Post = require('../model/Post.model');

const create = async (value, user) => {
  try {
    const post = new Post({
      title: value.title,
      body: value.body,
      postedBy: user,
    });
    const savedPost = await post.save();
    if (savedPost) {
      const response = {
        status: 200,
        message: savedPost,
      };
      return response;
    }
  } catch (err) {
    const response = {
      status: 422,
      message: 'post not created!',
    };
    return response;
  }
};

const following = async (user) => {
  try {
    const posts = await Post.find({ postedBy: { $in: user.following } })
      .populate('postedBy', '_id email name')
      .populate('comments.postedBy', '_id name email')
      .sort('-createdAt');
    if (posts) {
      const response = {
        status: 200,
        message: posts,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'post not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'post not found!',
    };
    return response;
  }
};

const user = async (user) => {
  try {
    const posts = await Post.find({ postedBy: user._id })
      .populate('postedBy', '_id email name')
      .populate('comments.postedBy', '_id name email')
      .sort('-createdAt');
    if (posts) {
      const response = {
        status: 200,
        message: posts,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'post not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'post not found!',
    };
    return response;
  }
};

const like = async (postid, userid) => {
  try {
    const post = await Post.findByIdAndUpdate(postid, {
      $push: { likes: userid },
    }, {
      new: true,
    });
    if (post) {
      const response = {
        status: 200,
        message: post,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'post not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'post not found!',
    };
    return response;
  }
};

const unLike = async (postid, userid) => {
  try {
    const post = await Post.findByIdAndUpdate(postid, {
      $pull: { likes: userid },
    }, {
      new: true,
    });
    if (post) {
      const response = {
        status: 200,
        message: post,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'post not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'post not found!',
    };
    return response;
  }
};

const deletePost = async (postid, userid) => {
  try {
    const post = await Post.findOne({ _id: postid })
      .populate('postedBy', '_id');
    if (post) {
      if (post.postedBy._id.toString() === userid.toString()) {
        const deletePost = await post.remove();
        if (deletePost) {
          const response = {
            status: 200,
            message: 'Deleted successfully',
          };
          return response;
        }
      }
      const response = {
        status: 422,
        message: 'You can delete only your post',
      };
      return response;
    }
  } catch (err) {
    const response = {
      status: 422,
      message: 'post not found!',
    };
    return response;
  }
};

module.exports = {
  create,
  following,
  user,
  like,
  unLike,
  deletePost,
};
