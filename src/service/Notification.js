const User = require('../model/User.model');
const Notification = require('../model/Notification.model');

const request = async (userId, followId) => {
  try {
    const request = new Notification({
      requestedBy: userId,
    });
    const notify = await request.save();
    if (notify) {
      const user = await User.findByIdAndUpdate(followId, {
        $push: { notifications: notify },
      },
      { new: true });
      if (user) {
        const response = {
          status: 200,
          message: user,
        };
        return response;
      }
    }
    const response = {
      status: 422,
      message: 'notification cannot be created!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'notification cannot be created!',
    };
    return response;
  }
};

const getRequest = async (userId) => {
  try {
    const user = await User.findById({ _id: userId })
      .populate('notifications', '_id');
    if (user) {
      const notification = await Notification.find({ _id: { $in: user.notifications } })
        .populate('requestedBy', '_id, email, name');
      const response = {
        status: 200,
        message: notification,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'notification not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'notification not found!',
    };
    return response;
  }
};

module.exports = {
  request,
  getRequest,
};
