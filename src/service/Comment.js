const Comment = require('../model/Comment.model');
const Post = require('../model/Post.model');

const comment = async (postId, comment, user) => {
  try {
    const { text } = comment;
    const newComment = new Comment({ text, postedBy: user });
    await newComment.save();
    const post = await Post.findByIdAndUpdate(postId, {
      $push: { comments: newComment },
    }, {
      new: true,
    })
      .populate('comments', '_id, text');
    if (post) {
      const response = {
        status: 200,
        message: post,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'comment cannot be created!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'comment cannot be created!',
    };
    return response;
  }
};

const view = async (postId) => {
  try {
    const post = await Post.findById({ _id: postId })
      .populate('comments', '_id');
    const comment = await Comment.find({ _id: { $in: post.comments } })
      .sort('-createdAt');
    if (comment) {
      const response = {
        status: 200,
        message: comment,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'comment not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'comment not found!',
    };
    return response;
  }
};

const deleteComment = async (postId, commentId) => {
  try {
    const deleteComment = await Comment.findByIdAndRemove(commentId);
    const post = await Post.findByIdAndUpdate(postId, {
      $pull: { comments: commentId },
    }, {
      new: true,
    }).populate('comments', '_id, text');
    if (post && deleteComment) {
      const response = {
        status: 200,
        message: post,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'comment not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'comment not found!',
    };
    return response;
  }
};

const like = async (commentId, userId) => {
  try {
    const comment = await Comment.findByIdAndUpdate(commentId, {
      $push: { likes: userId },
    }, {
      new: true,
    });
    if (comment) {
      const response = {
        status: 200,
        message: comment,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'comment not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'comment not found!',
    };
    return response;
  }
};

const unLike = async (commentId, userId) => {
  try {
    const comment = await Comment.findByIdAndUpdate(commentId, {
      $pull: { likes: userId },
    }, {
      new: true,
    });
    if (comment) {
      const response = {
        status: 200,
        message: comment,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'comment not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'comment not found!',
    };
    return response;
  }
};

const add = async (commentId, comment, user) => {
  try {
    const { text } = comment;
    const newComment = new Comment({ text, postedBy: user });
    await newComment.save();
    const comments = await Comment.findByIdAndUpdate(commentId, {
      $push: { comments: newComment },
    }, {
      new: true,
    });
    if (comments) {
      const response = {
        status: 200,
        message: comment,
      };
      return response;
    }
    const response = {
      status: 422,
      message: 'comment not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'comment not found!',
    };
    return response;
  }
};

module.exports = {
  comment,
  view,
  deleteComment,
  like,
  unLike,
  add,
};
