const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../model/User.model');

const signUp = async (value) => {
  try {
    const {
      name, email, password, gender,
    } = value;
    const registeredUser = await User.findOne({ email });
    if (registeredUser) {
      const response = {
        status: 422,
        message: 'user already exists with that email!',
      };
      return response;
    }

    const hashedPassword = await bcrypt.hash(password, 12);
    const user = new User({
      email,
      password: hashedPassword,
      name,
      gender,
    });
    const savedUser = await user.save();
    if (savedUser) {
      const response = {
        status: 200,
        message: 'signup successfull!',
      };
      return response;
    }
  } catch (err) {
    const response = {
      status: 422,
      message: 'signup not successfull!',
    };
    return response;
  }
};

const login = async (email, password) => {
  try {
    const registeredUser = await User.findOne({ email });
    if (!registeredUser) {
      const response = {
        status: 422,
        message: 'Invalid Email or password',
      };
      return response;
    }
    const doMatch = await bcrypt.compare(password, registeredUser.password);
    if (doMatch) {
      const response = {
        status: 200,
        message: jwt.sign({ _id: registeredUser._id }, process.env.JWT_KEY),
      };
      return response;
    }
  } catch (err) {
    const response = {
      status: 422,
      message: 'login not successfull!',
    };
    return response;
  }
};

module.exports = {
  signUp,
  login,
};
