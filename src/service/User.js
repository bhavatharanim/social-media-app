const Post = require('../model/Post.model');
const User = require('../model/User.model');

const show = async (id) => {
  try {
    const registeredUser = await User.findOne({ _id: id })
      .select('-password');
    if (registeredUser) {
      const post = await Post.findOne({ _id: id })
        .populate('postedBy', '_id name');
      const response = {
        status: 200,
        message: {
          posts: post,
          user: registeredUser,
        },
      };
      return response;
    }
  } catch (err) {
    const response = {
      status: 422,
      message: 'You cannot view this user!',
    };
    return response;
  }
};

const follow = async (followid, userid) => {
  try {
    const followUser = await User.findByIdAndUpdate(followid, {
      $push: { followers: userid },
    }, {
      new: true,
    });
    if (followUser) {
      const user = await User.findByIdAndUpdate(userid, {
        $push: { following: followid },
      },
      { new: true }).select('-password');
      if (user) {
        const response = {
          status: 200,
          message: user,
        };
        return response;
      }
    }
    const response = {
      status: 422,
      message: 'user not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'user not found!',
    };
    return response;
  }
};

const unFollow = async (unfollowid, userid) => {
  try {
    const followUser = await User.findByIdAndUpdate(unfollowid, {
      $pull: { followers: userid },
    }, {
      new: true,
    });
    if (followUser) {
      const user = await User.findByIdAndUpdate(userid, {
        $pull: { following: unfollowid },
      },
      { new: true }).select('-password');
      if (user) {
        const response = {
          status: 200,
          message: user,
        };
        return response;
      }
    }
    const response = {
      status: 422,
      message: 'user not found!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'user not found!',
    };
    return response;
  }
};

const update = async (value, userid) => {
  try {
    const updates = {
      profileImage: value.profileImage,
      country: value.country,
      city: value.city,
      aboutMe: value.aboutMe,
    };
    const user = await User.findByIdAndUpdate(userid, {
      $set: updates,
    }, { new: true });
    if (user) {
      const response = {
        status: 200,
        message: 'user updation successfull!',
      };
      return response;
    }
    const response = {
      status: 200,
      message: 'user updation not successfull!',
    };
    return response;
  } catch (err) {
    const response = {
      status: 422,
      message: 'user not found!',
    };
    return response;
  }
};

module.exports = {
  show,
  follow,
  unFollow,
  update,
};
