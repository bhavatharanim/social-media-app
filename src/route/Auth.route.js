const express = require('express');
const authService = require('../service/Auth');
const { signupValidate, loginValidate } = require('../validation/Auth.validation');

const router = express.Router();

router.post('/signup', signupValidate, async (req, res) => {
  try {
    const response = await authService.signUp(req.body);
    return res.status(response.status).json({ message: response.message });
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.post('/login', loginValidate, async (req, res) => {
  try {
    const { email, password } = req.body;
    const response = await authService.login(email, password);
    return res.status(response.status).json({ message: response.message });
  } catch (err) {
    res.status(422).json({ error: 'Invalid email or password' });
  }
});

module.exports = router;
