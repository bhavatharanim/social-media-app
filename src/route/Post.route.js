const express = require('express');

const router = express.Router();
const login = require('../middleware/Login.middleware');
const postService = require('../service/Post');
const { createPostValidate } = require('../validation/Post.validation');

router.post('/', login, createPostValidate, async (req, res) => {
  try {
    const response = await postService.create(req.body, req.user);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.get('/', login, async (req, res) => {
  try {
    const response = await postService.following(req.user);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.get('/user', login, async (req, res) => {
  try {
    const response = await postService.user(req.user);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.put('/like', login, async (req, res) => {
  try {
    const postId = req.body.postid;
    const response = await postService.like(postId, req.user._id);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.put('/unlike', login, async (req, res) => {
  try {
    const postId = req.body.postid;
    const response = await postService.unLike(postId, req.user._id);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.delete('/:postid', login, async (req, res) => {
  try {
    const { postid } = req.params;
    const response = await postService.deletePost(postid, req.user._id);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

module.exports = router;
