const express = require('express');

const router = express.Router();
const userService = require('../service/User');
const login = require('../middleware/Login.middleware');
const { updateValidate } = require('../validation/User.validation');

router.get('/:id', login, async (req, res) => {
  try {
    const response = await userService.show(req.params.id);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.put('/follow', login, async (req, res) => {
  try {
    const response = await userService.follow(req.body.followid, req.user._id);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.put('/unfollow', login, async (req, res) => {
  try {
    const response = await userService.unFollow(req.body.followid, req.user._id);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.put('/update', login, updateValidate, async (req, res) => {
  try {
    const response = await userService.update(req.body, req.user._id);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

module.exports = router;
