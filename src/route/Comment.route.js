const express = require('express');

const router = express.Router();
const login = require('../middleware/Login.middleware');
const commentService = require('../service/Comment');

router.post('/', login, async (req, res) => {
  try {
    const postId = req.body.postid;
    const response = await commentService.comment(postId, req.body, req.user);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.get('/:postid', login, async (req, res) => {
  try {
    const response = await commentService.view(req.params.postid);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.delete('/:commentid', login, async (req, res) => {
  try {
    const postId = req.body.postid;
    const response = await commentService.deleteComment(postId, req.params.commentid);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.put('/like/:commentid', login, async (req, res) => {
  try {
    const commentId = req.params.commentid;
    const response = await commentService.like(commentId, req.user);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.put('/unlike/:commentid', login, async (req, res) => {
  try {
    const commentId = req.params.commentid;
    const response = await commentService.unLike(commentId, req.user._id);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.post('/:commentid', login, async (req, res) => {
  try {
    const commentId = req.params.commentid;
    const response = await commentService.add(commentId, req.body, req.user);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

module.exports = router;
