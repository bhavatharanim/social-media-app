const express = require('express');

const router = express.Router();
const login = require('../middleware/Login.middleware');
const notificationService = require('../service/Notification');

router.post('/', login, async (req, res) => {
  try {
    const { followid } = req.body;
    const response = await notificationService.request(req.user._id, followid);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

router.get('/', login, async (req, res) => {
  try {
    const response = await notificationService.getRequest(req.user._id);
    if (response) {
      return res.status(response.status).json({ message: response.message });
    }
  } catch (err) {
    res.status(422).json({ error: 'unexpected error occured try after some time' });
  }
});

module.exports = router;
