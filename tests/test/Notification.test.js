const assert = require('assert');
const { LocalStorage } = require('node-localstorage');

const Notification = require('../../src/model/Notification.model');
const User = require('../../src/model/User.model');

const localStorage = new LocalStorage('./scratch');

let res;
let done;
const testId = localStorage.getItem('userId');
let notificationId;

describe('Unit Tests : Test for routes/notification', () => {
  beforeEach(() => {
    jest.setTimeout(50000);
  });
  it('test case for create comment', () => {
    const request = new Notification({
      requestedBy: testId,
    });
    request.save();
    localStorage.setItem('notificationId', request._id);
    User.findByIdAndUpdate(testId, {
      $push: { notifications: request },
    }, {
      new: true,
    }).then(() => {
      assert.ok(res.statusCode === 200);
      done();
    })
      .catch(() => {
        expect(201);
      });
  });

  notificationId = localStorage.getItem('notificationId');

  it('Getting a notification', () => {
    Notification.findById(notificationId)
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  });
});
