const assert = require('assert');
const { LocalStorage } = require('node-localstorage');
const {
  dbConnect,
  dbDisconnect,
} = require('../db.utils');

const { fakeUserData } = require('../mockdata');
const User = require('../../src/model/User.model');

const localStorage = new LocalStorage('./scratch');

let res;
let done;
let findUser;

beforeAll(async () => {
  await dbConnect();
});
afterAll(async () => {
  await dbDisconnect();
});

describe('Unit Tests : Test for routes/auth.js', () => {
  beforeEach(() => {
    jest.setTimeout(10 * 10000);
  });

  it('should signup a new user', () => {
    const user = new User(fakeUserData);

    user.save();
    localStorage.setItem('userId', user._id);
    findUser = User.findOne(user);
    if (findUser._id === user._id) {
      assert.ok(res.statusCode === 200);
      done();
    } else {
      expect(201);
    }
  }, 50000);

  it('should signup only with unique email', () => {
    const user = new User(fakeUserData);

    user.save().then(() => {
      assert.ok(res.statusCode === 422);
      done();
    })
      .catch(() => {
        expect(201);
      });
  }, 50000);

  it('should login a unique user', () => {
    User.findOne({ email: fakeUserData.email })
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 50000);
});
