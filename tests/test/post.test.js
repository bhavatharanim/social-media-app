const assert = require('assert');
const { LocalStorage } = require('node-localstorage');

const Post = require('../../src/model/Post.model');

const localStorage = new LocalStorage('./scratch');

let res;
let done;
const testId = localStorage.getItem('userId');
let postId;

describe('Unit Tests : Test for routes/post', () => {
  beforeEach(() => {
    jest.setTimeout(500000);
  });
  it('Test case for create post', () => {
    const post = new Post({
      title: 'test',
      body: 'test',
      postedBy: testId,
    });

    post.save()
      .then(() => {
        localStorage.setItem('postId', post._id);
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  }, 500000);

  postId = localStorage.getItem('postId');

  it('Delete post', () => {
    const post = Post.findById(postId);
    if (post._id === { _id: postId }) {
      post.deleteOne();
      assert.ok(res.statusCode === 200);
      done();
    } else {
      expect(201);
    }
  });

  it('Like post', () => {
    Post.findByIdAndUpdate(postId, {
      $push: { likes: testId },
    }, {
      new: true,
    })
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  });

  it('Unlike post', () => {
    Post.findByIdAndUpdate(postId, {
      $pull: { likes: testId },
    }, {
      new: true,
    })
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  });

  it('Getting a post', () => {
    Post.findById(postId)
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  });

  it('My post', () => {
    Post.find({ postedBy: testId })
      .populate('postedBy', '_id name email')
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  });

  it('Following post', () => {
    Post.find({ postedBy: { $in: testId } })
      .populate('postedBy', '_id name email')
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  });
});
