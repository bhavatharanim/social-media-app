const assert = require('assert');
const { LocalStorage } = require('node-localstorage');

const Comment = require('../../src/model/Comment.model');
const Post = require('../../src/model/Post.model');

const localStorage = new LocalStorage('./scratch');

let res;
let done;
const testId = localStorage.getItem('testId');
const postId = localStorage.getItem('postId');
let commentId;

describe('Unit Tests : Test for routes/comment', () => {
  beforeEach(() => {
    jest.setTimeout(50000);
  });
  it('test case for create comment', () => {
    const newComment = new Comment({
      text: 'testing',
      postedBy: testId,
    });
    newComment.save();
    localStorage.setItem('commentId', newComment._id);
    Post.findByIdAndUpdate(postId, {
      $push: { comments: newComment },
    }, {
      new: true,
    }).then(() => {
      assert.ok(res.statusCode === 200);
      done();
    })
      .catch(() => {
        expect(201);
      });
  });

  commentId = localStorage.getItem('commentId');

  it('Delete comment', () => {
    const comment = Comment.findById(commentId);
    if (comment._id === { _id: commentId }) {
      comment.deleteOne();
      assert.ok(res.statusCode === 200);
      done();
    } else {
      expect(201);
    }
  });

  it('Like comment', () => {
    Comment.findByIdAndUpdate(commentId, {
      $push: { likes: testId },
    }, {
      new: true,
    })
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  });

  it('Unlike comment', () => {
    Comment.findByIdAndUpdate(commentId, {
      $pull: { likes: testId },
    }, {
      new: true,
    })
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  });

  it('Getting a comment', () => {
    Comment.findById(commentId)
      .then(() => {
        assert.ok(res.statusCode === 200);
        done();
      })
      .catch(() => {
        expect(201);
      });
  });
});
