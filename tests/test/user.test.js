const assert = require('assert');
const { LocalStorage } = require('node-localstorage');

const { fakeUserData } = require('../mockdata');
const User = require('../../src/model/User.model');
const Post = require('../../src/model/Post.model');

const localStorage = new LocalStorage('./scratch');

let res;
let done;
let testId;

describe('Unit Tests : Test for routes/user.js', () => {
  beforeEach(() => {
    jest.setTimeout(50000);
  });
  it('should display the details of a user', () => {
    User.findOne({ email: fakeUserData.email })
      .then((user) => {
        testId = user._id;
        localStorage.setItem('testId', testId);
        Post.find({ postedBy: user._id })
          .populate('postedBy', '_id name')
          .then(() => {
            assert.ok(res.statusCode === 200);
            done();
          });
      })
      .catch(() => {
        expect(201);
      });
  });

  it('should update the details of the logged in user', () => {
    const profileImage = 'someurl';
    const country = 'test';
    const city = 'test';
    const aboutMe = 'test';
    const updates = {
      profileImage,
      country,
      city,
      aboutMe,
    };
    User.findByIdAndUpdate(testId, {
      $set: updates,
    }, { new: true })
      .then(() => {
        assert.ok(res.statusCode === 200);
      })
      .catch(() => {
        expect(201);
      });
  });

  it('should follow a new user', () => {
    User.findByIdAndUpdate(testId, {
      $push: { followers: testId },
    }, {
      new: true,
    }, async (err, result) => {
      if (err) {
        expect(422);
      }
      if (result) {
        await User.findByIdAndUpdate(testId, {
          $push: { following: testId },
        },
        { new: true })
          .then(() => {
            assert.ok(res.statusCode === 200);
            done();
          })
          .catch(() => {
            expect(201);
          });
      }
    });
  });

  it('should unfollow a already followed user', () => {
    User.findByIdAndUpdate(testId, {
      $pull: { followers: testId },
    }, {
      new: true,
    }, async (err, result) => {
      if (err) {
        expect(422);
      }
      if (result) {
        await User.findByIdAndUpdate(testId, {
          $pull: { following: testId },
        },
        { new: true })
          .then(() => {
            assert.ok(res.statusCode === 200);
            done();
          })
          .catch(() => {
            expect(201);
          });
      }
    });
  });
});
