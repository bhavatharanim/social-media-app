# Social Media App Backend 
 
Build RESTful APIs for a social media application.

## Objective
Building a RESTful APIs for a social media application that can be used to create post and view posts of their friends.

## Features 
- User login SignUp
- User can fill up profile details, upload profile picture, update details
- User will have a feed where user can see posts of friends and post their own posts.
- List all the friends
- Accept/ decline Pending Friend Request

---
### Tech Stack 
> Node, Express, MongoDB, Heroku
 

Application is running on https://social-media-production.herokuapp.com/

### Postman link

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/ce7064fb0b2b18545260?action=collection%2Fimport)
